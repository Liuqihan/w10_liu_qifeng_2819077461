# 第10周-卷积神经网络分割任务
## 技能掌握
学习主流分割模型。

---
**`学习作业-FCN训练模型`**

> 问题描述

本作业以FCN为基础，构建一个FCN训练模型，要求学员实现代码中缺失的部分并使用自己的实现跑出比较好的结果。

学员需要将convert_fcn_dataset.py中的代码补全并生成对应的数据集文件上传到tinymind。学员需要在作业提供的代码基础上添加8X的FCN实现并进行训练。

详情见：[https://gitee.com/ai100/quiz-w9-code](https://gitee.com/ai100/quiz-w9-code)

> 解题提示

1. 数据集：本作业使用Pascal2 VOC2012的数据中，语义分割部分的数据作为作业的数据集。VOC网址：[http://host.robots.ox.ac.uk/pascal/VOC/voc2012/](http://host.robots.ox.ac.uk/pascal/VOC/voc2012/)

	本次作业不提供数据集下载，请学员自行到上述网址找到并下载数据，同时请仔细阅读VOC网站对于数据集的描述。其中本次作业使用VOC2012目录下的内容。作业数据集划分位于VOC2012/ImageSets/Segmentation中，分为train.txt 1464张图片和val.txt1449张图片。语义分割标签位于VOC2012/SegmentationClass,注意不是数据集中所有的图片都有语义分类的标签。 语义分割标签用颜色来标志不同的物体，该数据集中共有20种不同的物体分类，以1～20的数字编号，加上编号为0的背景分类，该数据集中共有21种分类。编号与颜色的对应关系如下：

		# class
		classes = ['background', 'aeroplane', 'bicycle', 'bird', 'boat',
		'bottle', 'bus', 'car', 'cat', 'chair', 'cow', 'diningtable',
		'dog', 'horse', 'motorbike', 'person', 'potted plant',
		'sheep', 'sofa', 'train', 'tv/monitor']

		# RGB color for each class
		colormap = [[0, 0, 0], [128, 0, 0], [0, 128, 0], [128, 128, 0], [0, 0, 128],
		[128, 0, 128], [0, 128, 128], [128, 128, 128], [64, 0, 0], [192, 0, 0],
		[64, 128, 0], [192, 128, 0], [64, 0, 128], [192, 0, 128],
		[64, 128, 128], [192, 128, 128], [0, 64, 0], [128, 64, 0],
		[0, 192, 0], [128, 192, 0], [0, 64, 128]]


2. 训练数据准备：训练数据需要预先打包成tfrecord格式，本步骤在本地完成。打包使用作业代码中的convert_fcn_dataset.py脚本进行。脚本内容已经删掉一部分，需要由学员自行补全缺失部分的代码。
	
		python3 convert_fcn_dataset.py --data_dir=/path/to/VOCdevkit/VOC2012/ --output_dir=./

本步骤最终生成的两个文件fcn_train.record,fcn_val.record分别在400MB左右，共800MB左右，如果最后的文件大小过大或过小，生成数据的过程可能有问题，请注意检查。

3. 模型：预训练模型使用tensorflow，modelzoo中的VGG16模型，请学员自行到modelzoo中查找并将该预训练模型放到tinymind上。网络有问题的学员，可以使用已经预先上传到tinymind的模型，数据集为ai100/vgg16.
模型代码以课程视频week9 FCN部分的代码进行了修改，主要是代码整理，添加了数据输入和结果输出的部分。代码参考：[https://gitee.com/ai100/quiz-w9-code.git](https://gitee.com/ai100/quiz-w9-code.git)在tinymind上新建一个模型，模型设置参考如下模型：[https://www.tinymind.com/ai100/quiz-w9-fcn](https://www.tinymind.com/ai100/quiz-w9-fcn)

运行过程中，模型每100个step会在/output/train下生成一个checkpoint，每200步会在/output/eval下生成四张验证图片。

> 批改标准

数据集准备完成-20分：

数据集中应包含train和val两个tfrecord文件，大小在400MB左右

模型训练完成-20分：

在tinymind运行log的输出中，可以看到如下内容：

	2018-01-04 11:11:20,088 - DEBUG - train.py:298 - step 1200 Current Loss: 101.153938293
	2018-01-04 11:11:20,088 - DEBUG - train.py:300 - [23.54] imgs/s
	2018-01-04 11:11:21,011 - DEBUG - train.py:307 - Model saved in file: ./out/train/model.ckpt-1200
	2018-01-04 11:11:21,018 - DEBUG - train.py:314 - validation generated at step [1200]
	2018-01-04 11:11:28,461 - DEBUG - train.py:298 - step 1210 Current Loss: 116.911231995
	2018-01-04 11:11:28,461 - DEBUG - train.py:300 - [19.11] imgs/s
	2018-01-04 11:11:35,356 - DEBUG - train.py:298 - step 1220 Current Loss: 90.7060165405

训练结果完成-20分：

训练完成之后，可以在**/output/eval下面生成验证的图片，其中val_xx_prediction.jpg**的图片为模型输出的预测结果，内容应可以对应相应的annotation和img。根据验证图片的内容，结果可能会有区别，但是肯定可以看到输出的结果是明显有意义的。

模型代码补全 -20分：

train.py中可以看到8x代码的实现。形式可能会有区别，但是有比较明显的三个上采样过程，两个2X，一个8X，及其结果的融合。

心得体会-20分 